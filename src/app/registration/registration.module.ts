import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {RegistrationComponent} from "./components/pages/registration/registration.component";



@NgModule({
  declarations: [
    RegistrationComponent
  ],
  exports: [
    RegistrationComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RegistrationModule { }
