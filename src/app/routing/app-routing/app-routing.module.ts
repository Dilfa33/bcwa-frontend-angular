import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginModule} from "../../login/login.module";
import {RegistrationModule} from "../../registration/registration.module";
import {RouterModule, Routes} from "@angular/router";

import {RegistrationComponent} from "../../registration/components/pages/registration/registration.component";
import {LoginComponent} from "../../login/components/pages/login/login.component";
import {HomeComponent} from "../../home/components/pages/home/home.component";
import {HomeModule} from "../../home/home.module";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "register", component: RegistrationComponent },
  { path: "home", component: HomeComponent },
  { path: '', redirectTo: "/home", pathMatch: "full" }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LoginModule,
    RegistrationModule,
    HomeModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
