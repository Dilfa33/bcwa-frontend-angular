import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from "./core/core.module";
import { LoginModule } from "./login/login.module";
import { AppRoutingModule } from "./routing/app-routing/app-routing.module";

import { AppComponent } from './app.component';
import {RegistrationModule} from "./registration/registration.module";


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    LoginModule,
    RegistrationModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
