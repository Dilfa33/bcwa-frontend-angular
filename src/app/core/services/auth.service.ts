import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loginUrl: string = ""

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    const body: HttpParams = new HttpParams()
        .set("username", username)
        .set("password", password);

    const headers: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/x-www-form-urlencoded"
    });

    return this.http.post(this.loginUrl, body.toString(), { headers });
  }
}
