import { Component } from '@angular/core';
import { AuthService } from "../../../../core/services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username: string = '';
  password: string = '';

  constructor(private authService: AuthService) { }

  login(): void {
    this.authService.login(this.username, this.password).subscribe(
      {
        next: (response) => {
          if (response.access_token) {
            localStorage.setItem("token", response.access_token);
          }
        },
        error: (error) => {
          console.error("Login failed: ", error);
        }
      }
    )
  }
}
